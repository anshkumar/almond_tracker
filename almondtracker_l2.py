##################################
# author: vedanshu
##################################

from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
from socketserver import StreamRequestHandler
from threading import Thread
from io import BytesIO
import threading 
import imutils
import sys
import cv2, queue, threading, time
from IPython import embed
import socket
import struct
import io
import tensorflow as tf
import numpy as np
import os
import random
import time
from functools import partial
import syslog
from time import sleep
import collections
from pathlib import Path
import json
from scipy.spatial import distance as dist
from scipy import spatial
from collections import OrderedDict
import logging
import math
from math import atan2, cos, sin, sqrt, pi
from sklearn.decomposition import IncrementalPCA
import glob
# pip3 install mysqlclient
import MySQLdb
import csv

BASE_INPUT = '/home/sort/videos/'

DEBUG_OUTPUT = False
BASE_PATH = '/home/sort/ved/almond_tracker/'
try:
    os.remove('data.csv')
except:
    pass 

WIDTH_OFFSET = 0
MASK_THRESHOLD = 0.5
       
DEBUG = True
# PIX2MM_SIZE = [0.0844444444]
PIX2MM_SIZE = [0.088]

NUM_CHANNELS = 1
USE_FITELLIPSE = False
GRID_SHAPE = (8,8) # (width, height)
IMAGE_SHAPE=(100, 100) # (width, height) Width has to be equal to height
MAX_DETECTION_IN_L2 = 10
MAX_L1_BATCH = 1

logging.basicConfig(filename="/home/sort/ved/almond_tracker/logfile.log",
                    format='%(asctime)s %(levelname)s %(message)s',
                    filemode='a')

logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

def profile(func):
    def wrap(*args, **kwargs):
        started_at = time.time()
        result = func(*args, **kwargs)
        print(func.__name__+': '+ str(time.time() - started_at))
        return result
    return wrap

def draw_label(image, point, label, font=cv2.FONT_HERSHEY_SIMPLEX,
               font_scale=0.5, thickness=2):
    size = cv2.getTextSize(label, font, font_scale, thickness)[0]
    x, y = point
        
    cv2.rectangle(image, (x, y - size[1]),
                  (x + size[0], y), (255, 0, 0), cv2.FILLED)
    cv2.putText(image, label, point, font, font_scale,
                (255, 255, 255), thickness)

def crop_center(img,cropx,cropy):
    y,x,ch = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)
    if starty >= 0 and starty+cropy >= 0 and startx >= 0 and startx+cropx >= 0 and starty <=y and starty+cropy <=y and startx <=x and startx+cropx <=x:
        if y < x:
            return img[starty:starty+cropy,startx:startx+cropx]
        else:
            return img[startx:startx+cropx,starty:starty+cropy]
    else:
        return img

class CentroidTracker:
    def __init__(self, maxDisappeared=2 , maxDistance=150):
        # initialize the next unique object ID along with two ordered
        # dictionaries used to keep track of mapping a given object
        # ID to its centroid and number of consecutive frames it has
        # been marked as "disappeared", respectively
        self.nextObjectID = [0 for i in range(NUM_CHANNELS)]
        self.objects = [OrderedDict() for i in range(NUM_CHANNELS)]
        self.objects_size = [OrderedDict() for i in range(NUM_CHANNELS)]
        self.objects_length = [OrderedDict() for i in range(NUM_CHANNELS)]
        self.objects_def_name = [OrderedDict() for i in range(NUM_CHANNELS)]
        self.disappeared = [OrderedDict() for i in range(NUM_CHANNELS)]
        if DEBUG:
            self.objects_mask = [OrderedDict() for i in range(NUM_CHANNELS)]
            self.objects_mask_debug = [OrderedDict() for i in range(NUM_CHANNELS)]
        self.totalDefective = 0
        self.totalDefectiveSized = 0


        # store the number of maximum consecutive frames a given
        # object is allowed to be marked as "disappeared" until we
        # need to deregister the object from tracking
        self.maxDisappeared = maxDisappeared

        # store the maximum distance between centroids to associate
        # an object -- if the distance is larger than this maximum
        # distance we'll start to mark the object as "disappeared"
        self.maxDistance = maxDistance
        self.totalDown = 0
        self.totalUp = 0
        self.target_size = 100
        self.db = MySQLdb.connect(user='sort_user',passwd="sort@123",db="info_almond")
        self.c = self.db.cursor()

    def register(self, centroid, _size, _length, def_name, def_conf, def_ratio, color_name, mask, only_mask, chan):
        # when registering an object we use the next available object
        # ID to store the centroid
        self.objects[chan][self.nextObjectID[chan]] = centroid
        self.objects_size[chan][self.nextObjectID[chan]] = [_size]
        self.objects_length[chan][self.nextObjectID[chan]] = [_length]
        self.objects_def_name[chan][self.nextObjectID[chan]] = [def_name]
        if DEBUG:
            self.objects_mask[chan][self.nextObjectID[chan]] = [mask]
            self.objects_mask_debug[chan][self.nextObjectID[chan]] = [only_mask]
        self.disappeared[chan][self.nextObjectID[chan]] = 0
        self.nextObjectID[chan] += 1

    def get_defect_by_priority(self, a_dictionary):
        return max(a_dictionary, key=a_dictionary.get)

    def deregister(self, objectID, chan):
        # to deregister an object ID we delete the object ID from
        # both of our respective dictionaries
        global DIA_THRESHOLD_MIN, DIA_THRESHOLD_MAX, OUT_PATH_L1, OUT_PATH_L2

        _defect_count = {'normal': 0, 'dissimilar': 0,'donki': 0,'shriveled': 0,'sui_taanch': 0,'single_taanch': 0,'double_taanch': 0,'shell': 0,'broken':0}

        overall_def_count = 0
        for i,_def in zip(range(len(self.objects_def_name[chan][objectID])), self.objects_def_name[chan][objectID]):
            if _def != 'normal':
                _defect_count[_def] += 1
                overall_def_count += 1

        if overall_def_count > 0:
            label_l2 = self.get_defect_by_priority(_defect_count)
        else:
            label_l2 = 'normal'

        try:
            if len(self.objects_size[chan][objectID]) > 22:
                temp_objects_size = self.objects_size[chan][objectID][0:-5]
                temp_objects_size_length = self.objects_length[chan][objectID][0:-5]
            else:
                temp_objects_size = self.objects_size[chan][objectID]
                temp_objects_size_length = self.objects_length[chan][objectID]
            sorted_objects_size_idx_prime = np.argsort(np.array(temp_objects_size).ravel()) #converts to single dimension
            max_object_size_prime = temp_objects_size[sorted_objects_size_idx_prime[-1]][0]
            min_object_size_prime = temp_objects_size[sorted_objects_size_idx_prime[0]][0]
            max_object_size_length_prime = temp_objects_size_length[sorted_objects_size_idx_prime[-1]][0]
        except Exception as e:
            print(e)
            del self.objects[chan][objectID]
            del self.objects_size[chan][objectID]
            del self.objects_length[chan][objectID]
            del self.disappeared[chan][objectID]
            del self.objects_def_name[chan][objectID]
            del self.objects_mask[chan][objectID]
            del self.objects_mask_debug[chan][objectID]
            return

        side_1 = max_object_size_prime
        side_2 = min_object_size_prime
        length_sz = max_object_size_length_prime

        y = np.array(self.objects_size[chan][objectID], dtype=np.float64)
        if y.shape[0] > 5:            
            self.totalDown += 1
            try:
                circumcircle_r = side_1               
            except:
                del self.objects[chan][objectID]
                del self.objects_size[chan][objectID]
                del self.objects_length[chan][objectID]
                del self.disappeared[chan][objectID]
                del self.objects_def_name[chan][objectID]
                del self.objects_mask[chan][objectID]
                del self.objects_mask_debug[chan][objectID]
                return
            if DEBUG:
                logger.debug("Size: %s id: %s cam: %s", 2*circumcircle_r*PIX2MM_SIZE[chan], objectID, chan)
                # print(np.array(self.objects_size[chan][objectID]).ravel().tolist())

            volume = math.pi*(length_sz*side_1*side_2)/6
            density = 1.086/2098737.7916560117
            g_mass = volume*density
            _counts = 28.3495/g_mass
            if _counts <=20:
                dia_mm = '18/20'
            elif _counts <=22:
                dia_mm = '20/22'
            elif _counts <=25:
                dia_mm = '23/25'
            elif _counts <=27:
                dia_mm = '25/27'
            elif _counts <=30:
                dia_mm = '27/30'
            elif _counts <=32:
                dia_mm = '30/32'
            elif _counts > 32:
                dia_mm = '32/34'
            # dia_mm = 2*circumcircle_r*PIX2MM_SIZE[chan]
            # embed()
            self.c.execute('''INSERT INTO almondtable (Company_id, Auction_id, Lot_number, Type, Filename, defect, side_1, camera_id, object_id, circumcircle_radius, length, volume, dia_mm) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)''', (str(self.com_id), str(self.auc_no),  str(self.lot), str(self.type), str(chan)+'_'+str(objectID)+ ".jpg", str(label_l2), str(side_1), str(chan), str(objectID), str(circumcircle_r), str(length_sz), str(volume), str(dia_mm)))
            self.db.commit()

            if DEBUG:
                for u, _s in zip(range(len(self.objects_mask_debug[chan][objectID])), self.objects_size[chan][objectID]):
                    picture = self.objects_mask_debug[chan][objectID][u]
                    picture = np.array(picture, dtype=np.uint8)
                    cv2.putText( picture, str(_s), (10,80), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255) , 2, cv2.LINE_AA)
                    cv2.imwrite(os.path.join(self.OUT_PATH_L1, str(chan)+'_'+str(objectID)+'_'+str(u)+ ".jpg"), cv2.cvtColor(picture, cv2.COLOR_RGB2BGR)) 
              

        del self.objects[chan][objectID]
        del self.objects_size[chan][objectID]
        del self.objects_length[chan][objectID]
        del self.disappeared[chan][objectID]
        del self.objects_def_name[chan][objectID] 
        del self.objects_mask[chan][objectID]
        del self.objects_mask_debug[chan][objectID]

    def update(self, rectangles, sizes, lengths, defects, obj_masks, debug_obj_masks):
        if not any(rectangles):   
            for chan in range(len(rectangles)):
                for objectID in list(self.disappeared[chan].keys()):
                    self.disappeared[chan][objectID] += 1

                    # if we have reached a maximum number of consecutive
                    # frames where a given object has been marked as
                    # missing, deregister it
                    if self.disappeared[chan][objectID] > self.maxDisappeared:
                        self.deregister(objectID, chan)
            return self.objects, self.objects_size

        for chan in range(len(rectangles)):
            rects = rectangles[chan]
            szs = sizes[chan]
            defs = defects[chan]
            lths = lengths[chan]
            if DEBUG:
                obj_mask = obj_masks[chan]
                obj_mask_debug = debug_obj_masks[chan]
            
            if len(rects) == 0:
                for objectID in list(self.disappeared[chan].keys()):
                    self.disappeared[chan][objectID] += 1

                    # if we have reached a maximum number of consecutive
                    # frames where a given object has been marked as
                    # missing, deregister it
                    if self.disappeared[chan][objectID] > self.maxDisappeared:
                        self.deregister(objectID, chan)
                continue

            # initialize an array of input centroids for the current frame
            inputCentroids = np.zeros((len(rects), 2), dtype="int")
            inputSizes = np.zeros((len(rects), 1), dtype=np.float64)
            inputLengths = np.zeros((len(rects), 1), dtype=np.float64)
            if DEBUG:
                inputMasks = np.zeros((len(rects), 100, 100, 3), dtype=np.float64)
                inputDebugMasks = np.zeros((len(rects), 600, 600, 3), dtype=np.float64)
            inputDefName = []
            objectCentroids = list(self.objects[chan].values())

            # loop over the bounding box rectangles
            for (i, (startX, startY, endX, endY)) in enumerate(rects):
                # use the bounding box coordinates to derive the centroid
                cX = int((startX + endX) / 2.0)
                cY = int((startY + endY) / 2.0)
                inputCentroids[i] = (cX, cY)

            for (i, _size) in enumerate(szs):
                inputSizes[i] = _size

            for (i, _length) in enumerate(lths):
                inputLengths[i] = _length

            for (i, _def_name) in enumerate(defs):
                inputDefName.append(_def_name)

            if DEBUG:
                for (i, _obj_mask) in enumerate(obj_mask):
                    inputMasks[i] = _obj_mask
                
                for (i, _debug_obj_mask) in enumerate(obj_mask_debug):
                    inputDebugMasks[i] = _debug_obj_mask

            # if we are currently not tracking any objects take the input
            # centroids and register each of them
            if len(self.objects[chan]) == 0:
                for i in range(0, len(inputCentroids)):
                    if DEBUG:
                        self.register(inputCentroids[i], inputSizes[i], inputLengths[i] , inputDefName[i], inputConf[i], inputRatio[i], inputColorName[i], inputMasks[i], inputDebugMasks[i], chan)
                    else:
                        self.register(inputCentroids[i], inputSizes[i], inputLengths[i] , inputDefName[i], inputConf[i], inputRatio[i], inputColorName[i], None, None, chan)

            # otherwise, are are currently tracking objects so we need to
            # try to match the input centroids to existing object
            # centroids
            else:
                # grab the set of object IDs and corresponding centroids
                objectIDs = list(self.objects[chan].keys())

                # compute the distance between each pair of object
                # centroids and input centroids, respectively -- our
                # goal will be to match an input centroid to an existing
                # object centroid

                # objectCentroids will be along the row
                # inputCentroids will be along the cols
                # print("objectCentroids: ", objectCentroids)
                # print("inputCentroids: ", inputCentroids)
                # D = dist.cdist(np.array(objectCentroids), inputCentroids)
                # # D of 2-D with objectCentroids along the row and inputCentroids along the cols
                # print("distance: ", D)

                # Calculate distance
                D = dist.cdist(np.array(objectCentroids), inputCentroids)

                # Calculate distance in forward direction only
                # D = np.zeros((len(objectCentroids), len(inputCentroids)))
                # for r, oc in zip(range(len(objectCentroids)), objectCentroids):
                #     for c, ic in zip(range(len(inputCentroids)), inputCentroids):
                #         if oc[1] <= ic[1]:
                #             D[r,c] = dist.euclidean(oc, ic)
                #         else:
                #             _ic = ic.copy()
                #             _ic[1] += 2*self.image_height
                #             D[r,c] = dist.euclidean(oc, _ic)


                # in order to perform this matching we must (1) find the
                # smallest value in each row and then (2) sort the row
                # indexes based on their minimum values so that the row
                # with the smallest value as at the *front* of the index
                # list
                # Returns the indices that would sort an array.
                rows = D.min(axis=1).argsort()

                # next, we perform a similar process on the columns by
                # finding the smallest value in each column and then
                # sorting using the previously computed row index list
                cols = D.argmin(axis=1)[rows]

                # in order to determine if we need to update, register,
                # or deregister an object we need to keep track of which
                # of the rows and column indexes we have already examined
                usedRows = set()
                usedCols = set()

                # loop over the combination of the (row, column) index
                # tuples
                for (row, col) in zip(rows, cols):

                    # if we have already examined either the row or
                    # column value before, ignore it
                    if row in usedRows or col in usedCols:
                        continue

                    # if the distance between centroids is greater than
                    # the maximum distance, do not associate the two
                    # centroids to the same object
                    if D[row, col] > self.maxDistance:
                        # self.deregister(objectID, chan)
                        continue

                    # otherwise, grab the object ID for the current row,
                    # set its new centroid, and reset the disappeared
                    # counter
                    objectID = objectIDs[row]
                    self.objects[chan][objectID] = inputCentroids[col]
                    self.objects_size[chan][objectID].append(inputSizes[col])
                    self.objects_length[chan][objectID].append(inputLengths[col])
                    self.objects_def_name[chan][objectID].append(inputDefName[col])
                    if DEBUG:
                        self.objects_mask[chan][objectID].append(inputMasks[col])
                        self.objects_mask_debug[chan][objectID].append(inputDebugMasks[col])
                    self.disappeared[chan][objectID] = 0

                    # indicate that we have examined each of the row and
                    # column indexes, respectively
                    usedRows.add(row)
                    usedCols.add(col)

                # compute both the row and column index we have NOT yet
                # examined
                unusedRows = set(range(0, D.shape[0])).difference(usedRows)
                unusedCols = set(range(0, D.shape[1])).difference(usedCols)

                # in the event that the number of object centroids is
                # equal or greater than the number of input centroids
                # we need to check and see if some of these objects have
                # potentially disappeared
                if D.shape[0] >= D.shape[1]:
                    # loop over the unused row indexes
                    for row in unusedRows:
                        # grab the object ID for the corresponding row
                        # index and increment the disappeared counter
                        objectID = objectIDs[row]
                        self.disappeared[chan][objectID] += 1

                        # check to see if the number of consecutive
                        # frames the object has been marked "disappeared"
                        # for warrants deregistering the object
                        if self.disappeared[chan][objectID] > self.maxDisappeared:
                            self.deregister(objectID, chan)

                # otherwise, if the number of input centroids is greater
                # than the number of existing object centroids we need to
                # register each new input centroid as a trackable object
                if D.shape[0] <= D.shape[1]:
                    for col in unusedCols:
                        if DEBUG:
                            self.register(inputCentroids[col], inputSizes[col], inputLengths[col], inputDefName[col], inputConf[col], inputRatio[col], inputColorName[col], inputMasks[col],inputDebugMasks[col], chan)
                        else:
                            self.register(inputCentroids[col], inputSizes[col], inputLengths[col], inputDefName[col], inputConf[col], inputRatio[col], inputColorName[col], None, None, chan)

        return self.objects, self.objects_size

class TrackableObject:
    def __init__(self, objectID, centroid,  size):
        self.objectID = objectID
        self.centroids = [centroid]
        self.size = size
        self.counted = False
        self.def_counted = False
        self.size_counted = False

class VideoCapture:
  def __init__(self, host, port):
    self.server_socket = socket.socket()
    self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    self.server_socket.bind(('0.0.0.0', port))
    self.server_socket.listen(0)
    self.server_socket.settimeout(5.0)
    self.host = host
    self.port = port
    self.stop = True
    global logger
    logger.debug("%s %s",host,port)
    self.q = queue.Queue()
    t = threading.Thread(target=self._reader)
    t.daemon = True
    t.start()
  def _reader(self):
    global logger
    while True:
        try:
            self.conn, _ = self.server_socket.accept()
            self.conn.settimeout(5.0)
            self.connection = self.conn.makefile('rb')
        except Exception as e:
            logger.error("ERROR from camera: %s:%s.", self.host,self.port)
            logger.error(e)
            continue
        while True:
          try:
            image_len = struct.unpack('<L', self.connection.read(struct.calcsize('<L')))[0]
            image_timestamp = struct.unpack('<d', self.connection.read(struct.calcsize('<d')))[0]
          except Exception as e:
            logger.error("ERROR from camera: %s:%s. Retrying in 2 seconds...", self.host,self.port)
            logger.error(e)
            break
          image_stream = io.BytesIO()
          try:
              image_stream.write(self.connection.read(image_len))
          except Exception as e:
              logger.error("ERROR from camera: %s:%s.", self.host,self.port)
              logger.error(e)
              break
          image_stream.seek(0)
          if not image_len:
             break
          frame = cv2.imdecode(np.fromstring(image_stream.getvalue(), dtype=np.uint8), 1)
          frame = frame[:, :, ::-1]
          # frame = np.rot90(frame)
          frame = cv2.UMat(frame).get()
          if not self.stop:
              self.q.put([frame, image_timestamp])
          else:
              self.q = queue.Queue()
  def read(self):
    img, _time = self.q.get()
    return img, _time
  def release(self):
    self.cap.release()

class ModelLoader:
    def __init__(self):
        self.detection_graph_merged = tf.Graph()
        self.detection_graph1 = tf.Graph()
        self.detection_graph2 = tf.Graph()
        self.new_model = False
        self.commodity = "almond"

    def load_graph_merged(self):
        with self.detection_graph_merged.as_default():
            # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.20)
            gpu_options = tf.GPUOptions(allow_growth=True)
            self.tf_sess1 = tf.Session(graph=self.detection_graph_merged, config=tf.ConfigProto(gpu_options=gpu_options))
            model = tf.saved_model.loader.load(self.tf_sess1, ["serve"],BASE_PATH+ "saved_model")
    
            self.tf_input = self.tf_sess1.graph.get_tensor_by_name('ved/level1/import/image_tensor:0')
            self.tf_boxes_l1 = self.tf_sess1.graph.get_tensor_by_name('detection_boxes_l1:0')
            self.tf_scores_l1 = self.tf_sess1.graph.get_tensor_by_name('detection_scores_l1:0')
            self.tf_classes_l1 = self.tf_sess1.graph.get_tensor_by_name('detection_classes_l1:0')
            self.tf_masks_l1 = self.tf_sess1.graph.get_tensor_by_name('detection_masks_l1:0')
            self.tf_boxes_l2 = self.tf_sess1.graph.get_tensor_by_name('detection_boxes_l2:0')
            self.tf_scores_l2 = self.tf_sess1.graph.get_tensor_by_name('detection_scores_l2:0')
            self.tf_classes_l2 = self.tf_sess1.graph.get_tensor_by_name('detection_classes_l2:0')

    def _get_frozen_graph(self, graph_file):
        """Read Frozen Graph file from disk."""
        with tf.gfile.GFile(graph_file, "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        return graph_def

    def load_graph_l1(self):
        _dir = BASE_PATH+self.commodity
        _model_ver = sorted(os.listdir(_dir))[-1]
        logger.debug("MODEL VERSION for l1: %s", _model_ver)

        trt_graph1 = self._get_frozen_graph(_dir+'/'+_model_ver+'/l1/frozen_inference_graph.pb')

        with tf.device('/gpu:0'):
            [self.tf_input_l1, self.tf_scores_l1, self.tf_boxes_l1, self.tf_classes_l1, self.tf_num_detections_l1, self.tf_masks_l1] = tf.import_graph_def(trt_graph1, 
                                return_elements=['image_tensor:0', 'detection_scores:0', 
                                'detection_boxes:0', 'detection_classes:0','num_detections:0','detection_masks:0'])

        self.tf_sess1 = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))

        _image_temp = np.random.randint(low=0, high=255, size=(1,1232,640,3), dtype=np.uint8)
        self.tf_sess1.run([self.tf_scores_l1, self.tf_boxes_l1, self.tf_classes_l1, self.tf_num_detections_l1, self.tf_masks_l1], feed_dict={self.tf_input_l1: _image_temp})

    def load_graph_l2(self):
        _dir = BASE_PATH+self.commodity
        _model_ver = sorted(os.listdir(_dir))[-1]
        logger.debug("MODEL VERSION for l2: %s", _model_ver)

        trt_graph2 = self._get_frozen_graph(_dir+'/'+_model_ver+'/l2/frozen_inference_graph.pb')

        with tf.device('/gpu:1'):
            [self.tf_input_l2, self.tf_scores_l2, self.tf_boxes_l2, self.tf_classes_l2, self.tf_num_detections_l2] = tf.import_graph_def(trt_graph2, 
                                return_elements=['image_tensor:0', 'detection_scores:0', 
                                'detection_boxes:0', 'detection_classes:0','num_detections:0'])
        
        self.tf_sess2 = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
        _image_temp = np.random.randint(low=0, high=255, size=(1,200,200,3), dtype=np.uint8)
        self.tf_sess2.run([self.tf_scores_l2, self.tf_boxes_l2, self.tf_classes_l2, self.tf_num_detections_l2], feed_dict={self.tf_input_l2: _image_temp})

    def unload_graph(self):
        try:
            tf.reset_default_graph()
        except:
            return    

    def test_gpu(self):
        if not tf.test.is_gpu_available():
            os.system("sudo /home/sort/ved/NVIDIA-Linux-x86_64-455.23.04.run --silent --no-x-check")
        return tf.test.is_gpu_available()    

class CamInfer():
    def __init__(self, *args, **kwargs):
        self.ct = CentroidTracker()
        self.prob = [0.8,0.5,0.98,0.85]
        self.str_label = ['dissimilar','donki','shriveled','taanch']

        self.trackableObjects = [{} for i in range(NUM_CHANNELS)]
        self.worker_queue_process_cpu = queue.Queue()
        self.worker_queue_process_l1 = queue.Queue()
        self.worker_queue_infer_l2 = queue.Queue()
        self.model = None
        self.cap1 = VideoCapture('192.168.0.63', 8080)
        self.stop = True
        t = threading.Thread(target=self._infer)
        t.daemon = True
        self.q = queue.Queue()
        t.start()

    def read(self):
        return self.q.get()

    def _createMask(self, height, width, pts):
        mask = np.zeros((height, width), np.uint8)
        cv2.drawContours(mask, [pts], -1, (255, 255, 255), -1, cv2.LINE_AA)
        return mask

    def _non_max_suppression(self, boxes, probs=None, nms_threshold=0.3):
        """Non-max suppression

        Arguments:
            boxes {np.array} -- a Numpy list of boxes, each one are [x1, y1, x2, y2]
        Keyword arguments
            probs {np.array} -- Probabilities associated with each box. (default: {None})
            nms_threshold {float} -- Overlapping threshold 0~1. (default: {0.3})

        Returns:
            list -- A list of selected box indexes.
        """
        # if there are no boxes, return an empty list
        if len(boxes) == 0:
            return []

        # if the bounding boxes are integers, convert them to floats -- this
        # is important since we'll be doing a bunch of divisions
        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        # initialize the list of picked indexes
        pick = []

        # grab the coordinates of the bounding boxes
        y1 = boxes[:, 0]
        x1 = boxes[:, 1]
        y2 = boxes[:, 2]
        x2 = boxes[:, 3]

        # compute the area of the bounding boxes and grab the indexes to sort
        # (in the case that no probabilities are provided, simply sort on the
        # bottom-left y-coordinate)
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = y2

        # if probabilities are provided, sort on them instead
        if probs is not None:
            idxs = probs

        # sort the indexes
        idxs = np.argsort(idxs)

        # keep looping while some indexes still remain in the indexes list
        while len(idxs) > 0:
            # grab the last index in the indexes list and add the index value
            # to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)

            # find the largest (x, y) coordinates for the start of the bounding
            # box and the smallest (x, y) coordinates for the end of the bounding
            # box
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])

            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)

            # compute the ratio of overlap
            overlap = (w * h) / area[idxs[:last]]

            # delete all indexes from the index list that have overlap greater
            # than the provided overlap threshold
            idxs = np.delete(idxs, np.concatenate(([last],
                                                   np.where(overlap > nms_threshold)[0])))
        # return only the bounding boxes indexes
        return pick

    def _getPointAfterRotate(self, inputpoint,center,angle, startX=0, startY=0):
        # https://answers.opencv.org/question/196032/how-to-find-the-real-width-and-height-of-contours/
        preturn_x = (inputpoint[0] - center[0])*cos(-angle) - (inputpoint[1] - center[1])*sin(-angle)+center[0]
        preturn_y = (inputpoint[0] - center[0])*sin(-angle) + (inputpoint[1] - center[1])*cos(-angle)+center[1]
        return (int(preturn_x + startX), int(preturn_y + startY))

    def _getOrientation(self, pts):
        # pip3 install opencv-contrib-python
        # https://docs.opencv.org/3.4/d1/dee/tutorial_introduction_to_pca.html
        sz = len(pts)
        data_pts = np.empty((sz, 2), dtype=np.float64)
        for i in range(data_pts.shape[0]):
            data_pts[i,0] = pts[i,0,0]
            data_pts[i,1] = pts[i,0,1]
        # Perform PCA analysis
        mean = np.empty((0))
        mean, eigenvectors, eigenvalues = cv2.PCACompute2(data_pts, mean)
        # Store the center of the object
        cntr = (int(mean[0,0]), int(mean[0,1]))
        # ipca = IncrementalPCA(n_components=2, batch_size=3)
        # ipca.fit_transform(data_pts)
        # mean = ipca.mean_
        # eigenvectors = ipca.components_
        # eigenvalues = ipca.explained_variance_
        # # Store the center of the object
        # cntr = (int(mean[0]), int(mean[1]))
        angle = atan2(eigenvectors[0,1], eigenvectors[0,0]) # orientation in radians
        
        return cntr, angle

    def centeroidnp(self, arr):
        length = arr.shape[0]
        sum_x = np.sum(arr[:, 0])
        sum_y = np.sum(arr[:, 1])
        return sum_x/length, sum_y/length

    def _getcategories(self, roi, roi_size):
        if roi is None:
            logger.debug("roi is None")
            return 0
        roi = roi.astype(np.uint8)
        roi = cv2.cvtColor(roi, cv2.COLOR_RGB2HSV)[:,:, :2]
        cts_fruit = np.array([27.49924661, 129.5923013])

        Z = roi.reshape((-1,2))
        Z = Z[Z[:,0] != 0]
        Z = np.float32(Z)

        return spatial.distance.cosine(self.centeroidnp(Z), cts_fruit)*100

    def get_priority(self, defect_name):
        priority = ['normal', 'dissimilar','donki','shriveled','sui_taanch','single_taanch','double_taanch','shell','broken']
        return priority.index(defect_name)

    def _process_cpu(self):
        global videoWriter_lst

        while True:
            item = self.worker_queue_process_cpu.get()
            image, boxes_l1, scores_l1, classes_l1, masks_l1, boxes_l2, scores_l2, classes_l2 = item
            _chan = 0
            _tic = time.time()
            boxes = boxes_l1  # index by 0 to remove batch dimension
            scores = scores_l1
            classes = classes_l1
            masks = masks_l1

            org_image = image.copy()

            rects = [[] for i in range(NUM_CHANNELS)]
            locations = [[] for i in range(NUM_CHANNELS)]
            sizes = [[] for i in range(NUM_CHANNELS)]
            defectives = [[] for i in range(NUM_CHANNELS)]
            obj_mask = [[] for i in range(NUM_CHANNELS)]
            obj_mask_debug = [[] for i in range(NUM_CHANNELS)]
            lengths = [[] for i in range(NUM_CHANNELS)]
            defects = [[] for i in range(NUM_CHANNELS)]

            for i in range(boxes_l2.shape[0]):
                box = boxes[i]
                box = np.round(box).astype(int)
                (startY_l1, startX_l1, endY_l1, endX_l1) = box.astype("int")

                if scores[i] > 0.5:
                    boxW_l1 = endX_l1 - startX_l1
                    boxH_l1 = endY_l1 - startY_l1

                    roi_l1 = image[startY_l1:endY_l1, startX_l1:endX_l1]
                    mask = masks[i]
                    mask = cv2.resize(mask, (boxW_l1, boxH_l1))
                    mask_bgd = (mask < MASK_THRESHOLD)
                    mask = (mask > MASK_THRESHOLD)

                    _mask = mask*255
                    _mask = _mask.astype(np.uint8)
                    contours, hierarchy = cv2.findContours(_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
                    areas = [cv2.contourArea(c) for c in contours]
                    try:
                        max_index = np.argmax(areas)
                    except:
                        continue
                    cnt=contours[max_index]

                    (x_circle,y_circle),radius_circle = cv2.minEnclosingCircle(cnt)
                    center_circle = (int(x_circle),int(y_circle))
                    radius_circle = int(radius_circle)
                    
                    if (startX_l1 + x_circle - radius_circle) > WIDTH_OFFSET and (startX_l1 + x_circle + radius_circle) < (image.shape[1] - WIDTH_OFFSET):
                        
                        x_size,y_size,w_size,h_size = cv2.boundingRect(cnt)

                        if USE_FITELLIPSE:
                            ellipse = cv2.fitEllipse(cnt)

                        mask_bgd = (mask_bgd*255).astype(np.uint8)
                        _roi_l1 = roi_l1.copy()
                        roi_l1_masked = cv2.bitwise_and(roi_l1,roi_l1,
                            mask = (mask*255).astype(np.uint8))
                        _roi_l1_masked = roi_l1_masked.copy()

                        if USE_FITELLIPSE:
                            cv2.ellipse(roi_l1, ellipse, (0,255,255), 2)
                        else:
                            xmin = 99999
                            xmax = 0
                            ymin = 99999
                            ymax = 0
                            cntr, angle = self._getOrientation(cnt)
                            for v in range(cnt.shape[0]):
                                cnt[v][0][0], cnt[v][0][1] = self._getPointAfterRotate((cnt[v][0][0], cnt[v][0][1]), cntr ,angle);
                                if (cnt[v][0][0] < xmin):
                                    xmin = cnt[v][0][0]
                                if (cnt[v][0][0] > xmax):
                                    xmax = cnt[v][0][0]
                                if (cnt[v][0][1] < ymin):
                                    ymin = cnt[v][0][1]
                                if (cnt[v][0][1] > ymax):
                                    ymax = cnt[v][0][1]

                            lt = (xmin,ymin);
                            ld = (xmin,ymax);
                            rd = (xmax,ymax);
                            rt = (xmax,ymin);  

                            lt = self._getPointAfterRotate(lt, cntr, -angle);
                            ld = self._getPointAfterRotate(ld, cntr, -angle);
                            rd = self._getPointAfterRotate(rd, cntr, -angle);
                            rt = self._getPointAfterRotate(rt, cntr, -angle);

                            cv2.line( roi_l1_masked, lt, ld, (0,255,255), 2, cv2.LINE_AA)
                            cv2.line( roi_l1_masked, lt, rt, (0,255,255), 2, cv2.LINE_AA)
                            cv2.line( roi_l1_masked, rd, ld, (0,255,255), 2, cv2.LINE_AA)
                            cv2.line( roi_l1_masked, rd, rt, (0,255,255), 2, cv2.LINE_AA)

                        stack = np.hstack((_roi_l1, roi_l1_masked))
                        try:
                            stack, _, _, _ = self._getSquareImage(stack)
                        except:
                            continue
                        
                        if int(classes_l1[i]) == 2 and scores_l1[i]>0.9:
                            defect_l2 = "broken"
                        elif int(classes_l1[i]) == 3 and scores_l1[i]>0.95:
                            defect_l2 = "shell"
                        else:
                            defect_l2 = "normal"

                        defect_of_contour = []
                        for j in range(boxes_l2.shape[1]):#defect-id
                            _label_l2 = self.str_label[int(classes_l2[i][j])-1]

                            if scores_l2[i][j] > self.prob[int(classes_l2[i][j])-1]:
                                box_l2 = boxes_l2[i][j]
                                box_l2 = np.round(box_l2).astype(int)
                                (startY_l2, startX_l2, endY_l2, endX_l2) = box_l2.astype("int")
                                boxW_l2 = endX_l2 - startX_l2
                                boxH_l2 = endY_l2 - startY_l2
                                ratio = (boxW_l2*boxH_l2)/(boxW_l1*boxH_l1)

                                if int(classes_l2[i][j]) == 4 :
                                    # area_of_taanch = convex_hull(single_contour)
                                    print(ratio)
                                    if ratio<0.05:
                                        #sui_taanch
                                        sub_category = "sui_taanch"
                                    elif ratio>=0.05 and ratio<0.20:
                                        #single_taanch
                                        sub_category = "single_taanch"
                                    elif ratio>=0.20:
                                        #double_taanch
                                        sub_category = "double_taanch"
                                else:
                                    sub_category = _label_l2
                                defect_of_contour.append(sub_category)

                        for j in range(len(defect_of_contour)):
                            if self.get_priority(defect_of_contour[j])>self.get_priority(defect_l2):
                                defect_l2=defect_of_contour[j]
                        defects[_chan].append(defect_l2)
                        rects[_chan].append((startX_l1, startY_l1, endX_l1, endY_l1))
                        _min_size = min(dist.cdist([lt], [ld]), dist.cdist([rd], [ld]))[0][0]
                        _max_size = max(dist.cdist([lt], [ld]), dist.cdist([rd], [ld]))[0][0]
                        if USE_FITELLIPSE:
                            sizes[_chan].append(min(ellipse[1]))
                        else:
                            sizes[_chan].append(_min_size)
                            lengths[_chan].append(_max_size)
                        cv2.putText(stack, str(round(sizes[_chan][-1], 2)),(10,40), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255) , 2, cv2.LINE_AA) 

                        if DEBUG:
                            _roi_l1_masked, _, _, _ = self._getSquareImageScaled(_roi_l1_masked, 100)
                            obj_mask[_chan].append(_roi_l1_masked)
                            obj_mask_debug[_chan].append(stack)
            # print("L1 post process time: ", time.time()- tic)
            if DEBUG:
                objects, objects_size = self.ct.update(rects, sizes, lengths, defects, obj_mask, obj_mask_debug)
            else:
                objects, objects_size = self.ct.update(rects, sizes, lengths, defects, None, None)

            for (objectID, centroid), (_, _size) in zip(objects[_chan].items(), objects_size[_chan].items()):
                # check to see if a trackable object exists for the current
                # object ID
                to = self.trackableObjects[_chan].get(objectID, None)

                # if there is no existing trackable object, create one
                if to is None:
                    # print("Created new trackable object [id]: ", objectID)
                    to = TrackableObject(objectID, centroid, _size)

                # otherwise, there is a trackable object so we can utilize it
                # to determine direction
                else:
                    # to.centroids.append(centroid)
                    # to.defective |= _defective
                    # to.size = max(to.size, _size)
                    # print("to.size: ", to.size)
                    # print("centroids are: ", to.centroids)

                    # check to see if the object has been counted or not
                    # if not to.def_counted and not to.size_counted:
                    #     if to.defective == 1:
                    #         self.totalDefective += 1
                    #         to.def_counted = True
                    # if not to.size_counted and not to.def_counted:
                    #     if to.size < DIA_THRESHOLD_MIN or to.size > DIA_THRESHOLD_MAX:
                    #         self.totalDefectiveSized += 1
                    #         print("totalDefectiveSized: ", self.totalDefectiveSized)
                    #         to.size_counted = True
                    if not to.counted:
                        # self.ct.totalDown += 1
                        to.counted = True

                # store the trackable object in our dictionary
                self.trackableObjects[_chan][objectID] = to

                # draw both the ID of the object and the centroid of the
                # object on the output frame
                text = "{}".format(objectID)
                cv2.putText(image, text, (centroid[0] - 10, centroid[1] - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 0, 255), 3)
                cv2.circle(image, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

            # print("Total: ", self.ct.totalDown)
            # print("Defective: ", self.ct.totalDefective)
            # print("DefectiveSized: ", self.ct.totalDefectiveSized)
            # Drawing OFFSET lines

            # for pt in OFFSET:
            #     if pt[0] != 9999:
            #         cv2.line(image, (pt[0], 0), (pt[0], image.shape[0]), (0,0,255), 2)
            #         cv2.line(image, (pt[1], 0), (pt[1], image.shape[0]), (0,0,255), 2)
            cv2.line(image, (0, WIDTH_OFFSET), (image.shape[1], WIDTH_OFFSET), (0,0,255), 2) 
            cv2.line(image, (0, image.shape[0]-WIDTH_OFFSET), (image.shape[1], image.shape[0]-WIDTH_OFFSET), (0,0,255), 2) 

            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            if not self.q.empty():
                try:
                    self.q.get_nowait()   # discard previous (unprocessed) frame
                except:
                    pass
            self.q.put(image)
            self.worker_queue_process_cpu.task_done()
            i = time.time()
            #cv2.imwrite("BASE_PATH+cardamomtest/"+ str(i) + ".jpg", image)
            if DEBUG_OUTPUT:
                videoWriter_lst[0].write(image)
            # print("{:0.2f}".format(1/(time.time()-_tic)))

    def _getSquareImageScaled(self,  img, target_width = 400 ):
        width = img.shape[1]
        height = img.shape[0]

        square = np.zeros( (target_width, target_width, 3), dtype=np.uint8 )

        max_dim = width if width >= height else height
        scale = target_width / max_dim
        
        if ( width >= height ):
            width = target_width
            x = 0
            height = int(height * scale)
            y = int(( target_width - height ) / 2)
        else:
            y = 0
            height = target_width
            width = int(width * scale)
            x = int(( target_width - width ) / 2)

        square[y:y+height, x:x+width] = cv2.resize( img , (width, height) )

        return square, x, y, scale

    def _getSquareImageResized(self, img, target_width = 500 ):
        width = img.shape[1]
        height = img.shape[0]

        square = np.zeros( (target_width, target_width, 3), dtype=np.uint8 )

        max_dim = width if width >= height else height
        scale = target_width / max_dim

        if ( width >= height ):
            width = target_width
            x = 0
            height = int(height * scale)
            y = int(( target_width - height ) / 2)
        else:
            y = 0
            height = target_width
            width = int(width * scale)
            x = int(( target_width - width ) / 2)

        square[y:y+height, x:x+width] = cv2.resize( img , (width, height) )

        return square, x, y, scale

    def _getSquareImage(self,  img, target_width = 600 ):
        width = img.shape[1]
        height = img.shape[0]

        square = np.zeros( (target_width, target_width, 3), dtype=np.uint8 )
        xoff = int((target_width - width)/2)
        yoff = int((target_width - height)/2)
        # print(xoff)
        # print(yoff)
        # print(height)
        # print(width)

        square[yoff:yoff+height, xoff:xoff+width] = img
        return square, xoff, yoff, 1

    def _getSquareImageFactors(self, img_width, img_height, target_width = 500 ):
        max_dim = img_width if img_width >= img_height else img_height
        scale = target_width / max_dim
        if ( img_width >= img_height ):
            x = 0
            img_height = int(img_height * scale)
            y = int(( target_width - img_height ) / 2)
        else:
            y = 0
            img_width = int(img_width * scale)
            x = int(( target_width - img_width ) / 2)
        return x, y, scale

    def _gallery(self, array, ncols=7):
        # https://stackoverflow.com/a/42041135/4582711
        nindex, height, width, intensity = array.shape
        nrows = nindex//ncols
        assert nindex == nrows*ncols
        # want result.shape = (height*nrows, width*ncols, intensity)
        result = (array.reshape(nrows, ncols, height, width, intensity)
                  .swapaxes(1,2)
                  .reshape(height*nrows, width*ncols, intensity))
        return result    

    def _infer_l2(self):
        global GRID_SHAPE, IMAGE_SHAPE, MAX_DETECTION_IN_L2
        
        while True:
            item = self.worker_queue_infer_l2.get()

            image, grid_img, boxes_l1, scores_l1, classes_l1, masks_l1 = item
            try:
                boxes_l2, scores_l2, classes_l2, num_detections_l2 = self.ct.model.tf_sess2.run([self.ct.model.tf_boxes_l2, self.ct.model.tf_scores_l2, self.ct.model.tf_classes_l2, self.ct.model.tf_num_detections_l2], feed_dict={self.ct.model.tf_input_l2: grid_img})
            except Exception as e:
                if self.model is None:
                    logger.debug("Session is None.... ")
                    continue
                else:
                    raise e

            boxes_l2_arr = np.zeros((boxes_l1.shape[0], MAX_DETECTION_IN_L2,4))
            scores_l2_arr = np.zeros((boxes_l1.shape[0], MAX_DETECTION_IN_L2))
            classes_l2_arr = np.zeros((boxes_l1.shape[0], MAX_DETECTION_IN_L2))
            boxes_l2_arr_count = np.zeros((boxes_l1.shape[0]), dtype=np.int)

            for b in range(boxes_l2.shape[0]):
                _boxes_l2 = boxes_l2[b]
                _scores_l2 = scores_l2[b]
                _classes_l2 = classes_l2[b]
                _num_detections_l2 = int(num_detections_l2[b])

                grid_size = IMAGE_SHAPE[0]*GRID_SHAPE[0]
                _boxes_l2 = _boxes_l2 * np.array([grid_size, grid_size, grid_size, grid_size])
                boxes_l2_index = _boxes_l2/IMAGE_SHAPE[0]
                boxes_l2_index = boxes_l2_index.astype(int)
                _boxes_l2 = _boxes_l2.astype(int)

                pick = self._non_max_suppression(_boxes_l2, _scores_l2[:_num_detections_l2], 0.5)

                for i in pick:#  range(_scores_l2.shape[0])
                    if _scores_l2[i] > 0.3:
                        if boxes_l2_index[i][0] == boxes_l2_index[i][2] and boxes_l2_index[i][1] == boxes_l2_index[i][3]:
                            # without_clipping
                            y = boxes_l2_index[i][0]
                            x = boxes_l2_index[i][1]
                            original_index = b*GRID_SHAPE[0]*GRID_SHAPE[0] + y*GRID_SHAPE[0] + x # indexed zero
                            _ymin = _boxes_l2[i][0]
                            _xmin = _boxes_l2[i][1]
                            _ymax = _boxes_l2[i][2]
                            _xmax = _boxes_l2[i][3]
                        else:
                            # with_clipping
                            boxes1 = []
                            for _col in range(GRID_SHAPE[0]):
                                for _row in range(GRID_SHAPE[1]):
                                    boxes1.append([
                                        _row*IMAGE_SHAPE[1], _col*IMAGE_SHAPE[0], 
                                        (_row+1)*IMAGE_SHAPE[1], (_col+1)*IMAGE_SHAPE[0]])

                            boxes1 = np.array(boxes1)
                            # https://github.com/tensorflow/models/blob/master/research/ \
                            # object_detection/core/post_processing.py#L35
                            y1_min, x1_min, y1_max, x1_max = np.split(boxes1, 4, axis=1)
                            y2_min, x2_min, y2_max, x2_max = np.split(_boxes_l2[i], 4, axis=0)
                            intersection_xmin = np.maximum(x1_min, x2_min)
                            intersection_xmax = np.minimum(x1_max, x2_max)
                            intersection_ymin = np.maximum(y1_min, y2_min)
                            intersection_ymax = np.minimum(y1_max, y2_max)
                            intersection_area = np.maximum((intersection_xmax - intersection_xmin), 0) \
                                                * np.maximum((intersection_ymax - intersection_ymin), 0)

                            area1 = (y1_max - y1_min) * (x1_max - x1_min)
                            area2 = (y2_max - y2_min) * (x2_max - x2_min)

                            union_area = area1 + area2  - intersection_area 
                            # Adds a small epsilon to avoid divide-by-zero.
                            union_area = union_area.astype(np.float32) + 1e-8
                            intersection_area = intersection_area.astype(np.float32)

                            # Calculates IoU.
                            iou = intersection_area / union_area
                            iou = np.squeeze(iou)
                            _iou_index_max = np.argmax(iou)
                            _ymin = np.squeeze(intersection_ymin[_iou_index_max])
                            _xmin = np.squeeze(intersection_xmin[_iou_index_max])
                            _ymax = np.squeeze(intersection_ymax[_iou_index_max])
                            _xmax = np.squeeze(intersection_xmax[_iou_index_max])

                            y = int(_ymin/IMAGE_SHAPE[0])
                            x = int(_xmin/IMAGE_SHAPE[1])

                            original_index = b*GRID_SHAPE[0]*GRID_SHAPE[1] + \
                                             y*GRID_SHAPE[0] + x # indexed zero

                        try:
                            _x, _y, _scale = self._getSquareImageFactors(boxes_l1[original_index][3] - boxes_l1[original_index][1],
                                                                   boxes_l1[original_index][2] - boxes_l1[original_index][0],
                                                                   IMAGE_SHAPE[1] )
                        except Exception as e:
                            logger.error("ERROR in _getSquareImageFactors")
                            logger.error(e)
                            continue

                        _boxes_l2[i][0] = int((_ymin - y*IMAGE_SHAPE[1] -_y)/ _scale)
                        _boxes_l2[i][1] = int((_xmin - x*IMAGE_SHAPE[0] - _x)/ _scale)
                        _boxes_l2[i][2] = int((_ymax - y*IMAGE_SHAPE[1] - _y)/ _scale)
                        _boxes_l2[i][3] = int((_xmax - x*IMAGE_SHAPE[0] - _x)/ _scale)

                        startY_l1, startX_l1, endY_l1, endX_l1 = boxes_l1[original_index]
                        startY_l2, startX_l2, endY_l2, endX_l2 = _boxes_l2[i]

                        if boxes_l2_arr_count[original_index] < MAX_DETECTION_IN_L2:
                            boxes_l2_arr[original_index][boxes_l2_arr_count[original_index]] = [startY_l1 + startY_l2, startX_l1 + startX_l2, startY_l1 + endY_l2, startX_l1 + endX_l2]
                            scores_l2_arr[original_index][boxes_l2_arr_count[original_index]] = _scores_l2[i]
                            classes_l2_arr[original_index][boxes_l2_arr_count[original_index]] = _classes_l2[i]
                            boxes_l2_arr_count[original_index] += 1

            org_boxes_l2 = boxes_l2
            boxes_l2 = boxes_l2_arr
            scores_l2 = scores_l2_arr
            classes_l2 = classes_l2_arr

            self.worker_queue_process_cpu.put([image, boxes_l1, scores_l1, classes_l1, masks_l1, boxes_l2, scores_l2, classes_l2])
            self.worker_queue_infer_l2.task_done()
    
    def worker_process_l1(self):
        global GRID_SHAPE, IMAGE_SHAPE
        ncols = GRID_SHAPE[0]
        target_size = IMAGE_SHAPE[0]
        while True:
            item = self.worker_queue_process_l1.get()
            image, boxes_l1, scores_l1, classes_l1, masks_l1, num_detections_l1 = item

            boxes_l1 = boxes_l1[0]  # index by 0 to remove batch dimension
            scores_l1 = scores_l1[0]
            classes_l1 = classes_l1[0]
            masks_l1 = masks_l1[0]
            num_detections_l1 = int(num_detections_l1[0])

            boxes_pixels = []
            for i in range(num_detections_l1):
                # scale box to image coordinates
                box = boxes_l1[i] * np.array([image.shape[0], image.shape[1], image.shape[0], image.shape[1]])
                box = np.round(box).astype(int)
                boxes_pixels.append(box)

            boxes_pixels = np.array(boxes_pixels)

            pick = self._non_max_suppression(boxes_pixels, scores_l1[:num_detections_l1], 0.5)

            _boxes_l1 = []
            _scores_l1 = []
            _classes_l1 = []
            _masks_l1 = []
            img_lst = []

            for i in pick:
                if scores_l1[i] > 0.5:
                    box = boxes_pixels[i]
                    box = np.round(box).astype(int)
                    (startY, startX, endY, endX) = box.astype("int")
                    boxW = endX - startX
                    boxH = endY - startY
                    
                    if boxW*boxH <= 10:
                        continue

                    _boxes_l1.append(box)
                    _scores_l1.append(scores_l1[i])
                    _classes_l1.append(classes_l1[i])
                    _masks_l1.append(masks_l1[i])

                    mask = masks_l1[i]
                    mask = cv2.resize(mask, (boxW, boxH))
                    mask = (mask > MASK_THRESHOLD)
                    roi = image[startY:endY, startX:endX]
                    roi = cv2.bitwise_and(roi,roi,mask = (mask*255).astype(np.uint8))
                    _img, _x2, _y2, _scale2 = self._getSquareImageResized(roi, target_size)
                    img_lst.append(_img)

            boxes_l1 = np.array(_boxes_l1)
            scores_l1 = np.array(_scores_l1)
            classes_l1 = np.array(_classes_l1)
            masks_l1 = np.array(_masks_l1)

            grid_size = math.ceil(len(img_lst)/(ncols*ncols))
            grid_img = [] # Will always ensure to add a batch dimension at the beginning
            for i in range(grid_size):
                _grid_img = np.zeros((ncols*ncols,target_size,target_size,3), dtype=np.uint8)
                _temp_img_lst = img_lst[i*ncols*ncols:((i+1)*ncols*ncols)]
                _grid_img[:(len(_temp_img_lst))] = np.array(_temp_img_lst)
                _grid_img = self._gallery(_grid_img, ncols)
                grid_img.append(_grid_img)

            if len(grid_img) > 0:
                for i in range(MAX_L1_BATCH-len(grid_img)):
                    grid_img.append(np.zeros((ncols*target_size, ncols*target_size, 3), dtype=np.uint8))
                grid_img = np.array(grid_img)
                self.worker_queue_infer_l2.put([image, grid_img, boxes_l1, scores_l1, classes_l1, masks_l1])
            self.worker_queue_process_l1.task_done()
                
    def _infer(self):
        tic = time.time()
        threading.Thread(target=self._process_cpu).start()
        threading.Thread(target=self._infer_l2, daemon=True).start()
        threading.Thread(target=self.worker_process_l1, daemon=True).start()
        while True:
            if self.ct.model is None:
                logger.debug("Session is None.... ")
                logger.debug("Retrying in 5 seconds...")
                time.sleep(5)
                continue 
            if self.stop:
                logger.debug("Process is stopped...")
                time.sleep(1)
                continue
            image,_ = self.cap1.read() 
            tic = time.time()
            image = image[:, 500:-500]

            if image is None:
                raise Exception("Image is None...") 
                break

            try:
                boxes_l1, scores_l1, classes_l1, masks_l1, num_detections_l1 = self.ct.model.tf_sess1.run([self.ct.model.tf_boxes_l1, self.ct.model.tf_scores_l1, self.ct.model.tf_classes_l1, self.ct.model.tf_masks_l1, self.ct.model.tf_num_detections_l1], feed_dict={self.ct.model.tf_input_l1: image[None, ...]})
            except Exception as e:
                if self.ct.model is None:
                    logger.debug("Session is None.... ")
                    continue
                else:
                    raise e
            self.worker_queue_process_l1.put([image, boxes_l1, scores_l1, classes_l1, masks_l1, num_detections_l1])
            # print("fps: ", 1/(time.time()-tic))

videoWriter_lst = []

class Handler(BaseHTTPRequestHandler):
    modelLoader = ModelLoader()
    modelLoader.test_gpu()
    modelLoader.load_graph_l1()
    modelLoader.load_graph_l2()
    camInfer = CamInfer()
    camInfer.ct.model = modelLoader

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def handle(self):
        try:
            BaseHTTPRequestHandler.handle(self)
        except socket.error:
            pass

    def do_POST(self):
        global model_loaded, logger, videoWriter_lst, BASE_INPUT

        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)

        response = BytesIO()
        response.write(body)
        data = json.loads(response.getvalue().decode('utf-8'))
        action = data['action']
        logger.debug("------------------------------------------------")
        logger.debug("JSON received: ")
        logger.debug("%s",data)
        logger.debug("------------------------------------------------")

        if action == 'start':
            com_name = data['commodity']['name']
            Handler.camInfer.ct.com_id = data['commodity']['company_id']
            Handler.camInfer.ct.auc_no = data['commodity']['auction_no']
            Handler.camInfer.ct.lot = data['commodity']['lot_no']
            Handler.camInfer.ct.type = data['commodity']['type']

            model_loaded = True
            Handler.camInfer.cap1.stop = False
            Handler.camInfer.stop = False

            if DEBUG_OUTPUT:
                OUT_PATH_VID = os.path.join(BASE_INPUT, data['commodity']['company_id'], data['commodity']['auction_no'], data['commodity']['lot_no'], data['commodity']['type'], "out_vid")
                Path(OUT_PATH_VID).mkdir(parents=True, exist_ok=True)
                videoWriter1=cv2.VideoWriter(os.path.join(OUT_PATH_VID, 'output.avi'), cv2.VideoWriter_fourcc(*"MJPG"),30,(640, 1232))
                videoWriter_lst.append(videoWriter1)
            if DEBUG:
                OUT_PATH_L1 = os.path.join(BASE_INPUT, data['commodity']['company_id'], data['commodity']['auction_no'], data['commodity']['lot_no'], data['commodity']['type'], "out_img")
                Handler.camInfer.ct.OUT_PATH_L1 = OUT_PATH_L1
                Path(OUT_PATH_L1).mkdir(parents=True, exist_ok=True)
                files = glob.glob(OUT_PATH_L1+'/*.jpg')
                for f in files:
                    os.remove(f)

                OUT_PATH_L2 = os.path.join(OUT_PATH_L1, "l2")
                Handler.camInfer.ct.OUT_PATH_L2 = OUT_PATH_L2
                Path(OUT_PATH_L2).mkdir(parents=True, exist_ok=True)
                files = glob.glob(OUT_PATH_L2+'/*.jpg')
                for f in files:
                    os.remove(f)
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()

        elif action == 'count':
            try:
                model_commodity = Handler.camInfer.model.commodity
            except:
                json_str = {'total': 0, 'defective': 0, 'defectiveSized': 0, 'error': ERROR}
                json_str = json.dumps(json_str)
                self.send_response(200)
                self.send_header('Content-Type', 'application/json')
                self.end_headers()
                self.wfile.write(json_str.encode(encoding='utf_8'))
                return
            json_str = {'total': Handler.camInfer.ct.totalDown, 'defective': Handler.camInfer.ct.totalDefective, 'defectiveSized': Handler.camInfer.ct.totalDefectiveSized}
            json_str = json.dumps(json_str)            
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            self.wfile.write(json_str.encode(encoding='utf_8'))
        elif action == 'shutdown':
            os.system("/home/sort/ved/more/rpi_shutdown_m2.sh")
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            os.system("sudo poweroff")
            
        elif action == 'reboot':
            os.system("/home/sort/ved/more/rpi_reboot_m2.sh")
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            os.system("sudo reboot")
            
        else:
            Handler.camInfer.cap1.stop = True
            Handler.camInfer.stop = True
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()

    def do_GET(self):
        self.send_response(20)
        self.send_header('Content-type', 'multipart/x-mixed-replace; boundary=--jpgboundary')
        self.end_headers()
        global model_loaded
        if model_loaded == False:
            self.wfile.write("Model Not Loaded\r\n".encode())
            return

        while True:
            image = Handler.camInfer.read()
            r, buf = cv2.imencode(".jpg", image)
            self.wfile.write("--jpgboundary\r\n".encode())
            self.end_headers()
            self.wfile.write(bytearray(buf))
            self.wfile.write('\r\n'.encode())

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

ip = '0.0.0.0'
model_loaded = False

handler1 = partial(Handler)
server1 = ThreadedHTTPServer((ip, 9090), handler1)

target1 = Thread(target=server1.serve_forever,args=())

target1.start()

